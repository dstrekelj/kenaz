# Kenaz

Website template assignment for internship application.

## About

The project folder contains two directories:

* `dev`, containing various sources;

* `dist`, containing the latest 'production-ready' template files.

### The `dev` directory

The `dev` directory is organised by asset type. CSS and JS files, as well as assets such as fonts and images, are all placed in their own respective directories. The root of the `dev` directory contains several HTML modules that are sandboxes used for creation of specific template elements e.g. carousels.

Contents of the `dev` directory are handled by the `gulp` task runner to create singular CSS and JS files, and copy all assets to the `dist` directory in preparation for template distribution.

### The `dist` directory

The `dist` directory contains 'production-ready' styles, scripts, and assets. Their use is demonstrated with an example `index.html` file located in the root of the directory.

## Building

**NOTE:** This project requires [Node](https://nodejs.org/en/) and [Gulp](http://gulpjs.com/). Make sure to install and set up both tools before proceeding.

Clone the repository and make changes to files in the `dev` directory. Navigate to the location of `package.json` and run the following commands:

```
$ npm install
$ gulp
```

All asset files within the `dist` directory should now be regenerated, and the `index.file` should remain untouched by the task runner.
