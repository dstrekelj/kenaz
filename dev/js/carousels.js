/**
 * Carousel related scripting.
 */
(function carousels() {

  /**
   * Handles carousel behaviour creation.
   */
  var Carousel = (function () {
    
    /**
     * Makes an interactive carousel. Returns on error.
     *
     * @param element   Element of any 'carousel-*' class
     * @param duration  Transition duration in milliseconds
     * @param delay     Transition delay in milliseconds
     */
    function make(element, duration, delay) {
      
      if (element === null) {
        console.error("Missing 'element' argument");
        return;
      }
      
      if (duration === null) {
        console.error("Missing 'duration' argument");
        return;
      }
      
      if (delay === null) {
        console.error("Missing 'delay' argument");
        return;
      }
      
      if ($(element).hasClass("carousel-1") || $(element).hasClass("carousel-news")) {
        setup(element, duration, delay, 1);
      } else if ($(element).hasClass("carousel-2")) {
        setup(element, duration, delay, 2);
      } else if ($(element).hasClass("carousel-photo")) {
        // TODO
      } else {
        console.error("Element is not of any 'carousel' class");
        return;
      }
      
    }
    
    /**
     * Sets up interactive carousel behaviour.
     *
     * @param element   Element of any 'carousel-*' class
     * @param duration  Transition duration in milliseconds
     * @param delay     Transition delay in milliseconds
     * @param visible   Number of visible items in carousel
     */
    function setup(element, duration, delay, visible) {
      
      var ul = $(element).find("ul").first(),
          li = ul.find("li:first-child").first(),
          items = ul.find(".item"),
          left = $(element).find(".carousel-control-left").first(),
          right = $(element).find(".carousel-control-right").first();
      
      if (ul.length === 0) {
        console.error("Element is not parent to unordered list");
        return;
      }
      
      if (li.length === 0) {
        console.error("Unordered list is empty");
        return;
      }
      
      if (items.length === 0) {
        console.error("List item has no children of class '.item'");
        return;
      }
      
      if (left.length === 0) {
        console.error("Container has no element of class '.carousel-control-left'");
        return;
      }
      
      if (right.length === 0) {
        console.error("Container has no element of class '.carousel-control-right'");
        return;
      }
      
          // Amount by which margin is offset to show next carousel item
      var offset = parseInt(items.first().css("width")) + parseInt(li.css("margin-top")),
          // Minimum allowed margin offset before next item is empty space
          minOffset = -offset * (items.length - visible),
          // Current margin value
          margin = 0;
      
      /**
       * Calculates margin offset.
       *
       * @param m Current margin
       * @param o Offset per item
       * @param M Minimum allowed margin
       */
      function calculate(m, o, M) {
        if ((m + o) > 0) { return M; }
        if ((m + o) < M) { return 0; }
        return m + o;
      }
      
      /**
       * Transitions carousel items to the left.
       */
      function moveLeft() {
        $(ul).animate(
          { 'margin-left': margin = calculate(margin, offset, minOffset) },
          duration
        );
      }
      
      /**
       * Transitions carousel items to the right.
       */
      function moveRight() {
        $(ul).animate(
          { 'margin-left': margin = calculate(margin, -offset, minOffset) },
          duration
        );
      }
      
      /**
       * Executes default animation and returns animation handle.
       */
      function defaultAnimation() {
        return setInterval(moveRight, delay);
      }
      
      // Stores reference to default animation so it can be stopped.
      var animation = defaultAnimation();
      
      left.click(function () {
        clearInterval(animation);
        moveLeft();
        animation = defaultAnimation();
      });
      
      right.click(function () {
        clearInterval(animation);
        moveRight();
        animation = defaultAnimation();
      });
      
    }
    
    return {
      make : make
    };
    
  })();
  
  var duration = 500,
      delay = 5000;
  
  var cNews = $(".carousel-news");
  cNews.each(function (i,e) {
    Carousel.make(e, duration, delay);
  });
  
  var c1 = $(".carousel-1");
  c1.each(function (i,e) {
    Carousel.make(e, duration, delay);
  });
  
  var c2 = $(".carousel-2");
  c2.each(function (i,e) {
    Carousel.make(e, duration, delay);
  });
  
  /*
  var cPhoto = $(".carousel-photo");
  cPhoto.each(function (i, e) {
    Carousel.make(e, 500, 5000);
  });
  */

})();
