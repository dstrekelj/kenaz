/**
 * Header related scripting.
 */
(function header() {

  $('.search').hover(
    // Increase width property on hover
    function () {
      $(this)
        .find('.search-input')
        .addClass('animated')
        .animate({ width: '240px' }, 500);
    },
    // Decrease width property on hover (if input not focused)
    function () {
      var search_input = $(this).find('.search-input');
      if (!search_input.is(':focus')) {
        search_input
          .removeClass('animated')
          .animate({ width: '0px' }, 500);
      }
    }
  );

  $('.search-input').focusout(function () {
    $(this)
      .removeClass('animated')
      .animate({ width: '0px' }, 500);
  });

})();
