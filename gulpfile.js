var gulp = require('gulp');

// Plugins

var concat = require('gulp-concat'),
    del = require('del'),
    merge = require('gulp-merge'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify');

// Clean

gulp.task('clean', function () {
  return del([
    'dist/**/*',
    '!dist/index.html'
  ]);
});

// Vendor

gulp.task('vendor', function () {
  var jquery = gulp.src(
    'node_modules/jquery/dist/jquery.min.js',
    { base : 'node_modules/jquery/dist/' }
  ).pipe(gulp.dest('dist/js/'));
  
  var normalize = gulp.src(
    'node_modules/normalize.css/normalize.css',
    { base : 'node_modules/normalize.css/' }
  ).pipe(gulp.dest('dist/css/'));
  
  return merge(jquery, normalize);
});

// Assets

gulp.task('assets', function () {
  var fonts = gulp.src(
    'dev/fonts/**/*',
    { base : 'dev/' }
  ).pipe(gulp.dest('dist/'));
  
  var images = gulp.src(
    'dev/images/**/*',
    { base : 'dev/' }
  ).pipe(gulp.dest('dist/'));
  
  return merge(fonts, images);
});

// CSS

var cssSource = [
      'dev/css/fonts.css',
      'dev/css/grid.css',
      'dev/css/style.css',
      'dev/css/**/*'
    ],
    cssDestination = 'dist/css/';

gulp.task('css', function () {
  return gulp.src(cssSource)
    .pipe(concat('style.css'))
    .pipe(gulp.dest(cssDestination));
});

// JS

var jsSource = ['dev/js/static/top.js', 'dev/js/*.js', 'dev/js/static/bottom.js'],
    jsDestination = 'dist/js/';

gulp.task('js', function () {
  var js = gulp.src(jsSource)
    .pipe(concat('script.js'))
    .pipe(gulp.dest(jsDestination));
  
  var min = gulp.src(jsSource)
    .pipe(concat('script.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(jsDestination));
  
  return merge(js, min);
});

// Default task

gulp.task('default', ['clean', 'vendor', 'assets', 'css', 'js']);